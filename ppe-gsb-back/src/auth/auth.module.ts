import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UtilisateurModule } from '../utilisateur/utilisateur.module';
import { UtilisateurService } from '../utilisateur/utilisateur.service';

@Module({
  imports: [UtilisateurModule],
  providers: [AuthService, UtilisateurService],
  controllers: [AuthController],
})
export class AuthModule {}