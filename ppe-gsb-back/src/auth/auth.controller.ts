import { Controller, Get, All, Post, Body, Req, Inject, forwardRef, Param, Put, Delete, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import * as crypto from 'crypto-js';

@Controller('auth')
export class AuthController {
    constructor(@Inject(forwardRef(() => AuthService)) private auth: AuthService) {}

    @Post()
    async findAll(@Res() res, @Body() body) {
        const password = crypto.SHA512(body.password).toString(crypto.enc.Base64);
        const request = await this.auth.userService.repo.findOne({ where: { identifiant: body.identifiant, password: password } });
        if (request) {
            const resp = crypto.SHA512(request.nom+request.prenom).toString(crypto.enc.Base64);
            res.json(resp);
        }
        return null;
    }

    @Post('token')
    async find(@Res() res, @Body() body) {
        const request = await this.auth.validateUser(body.token);
        res.send(request);
    }
}
