
import { Injectable, Inject } from '@nestjs/common';
import { UtilisateurService } from '../utilisateur/utilisateur.service';
import * as crypto from 'crypto-js';

@Injectable()
export class AuthService {
  constructor(public readonly userService: UtilisateurService) {}

  async validateUser(token: string): Promise<any> {
    const users = await this.userService.repo.find();
    let user = {};
    users.forEach(element => {
      if (crypto.SHA512(element.nom+element.prenom).toString(crypto.enc.Base64) === token) {
        user = element;
      }
    })
    return user;
  }
}