import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Projet } from './projet.entity';

@Injectable()
export class ProjetService {
  constructor(
    @InjectRepository(Projet)
    public readonly repo: Repository<Projet>,
  ) {}
}