import { Entity, Column, PrimaryGeneratedColumn, Generated,  } from 'typeorm';

@Entity()
export class Projet {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column({length: 100, nullable: true, unique: true})
    nom: string;
    
    @Column({length: 100, nullable: true})
    createur: string;
    
    @Column({length: 100, nullable: true})
    client: string;
    
    @Column({type:"longtext", nullable: true})
    description: string;    
    
    @Column("date", {nullable: true})
    date_debut;

    @Column("date", {nullable: true})
    date_fin;

    @Column()
    id_chef_projet_utilisateur: number;

    @Column()
    id_status: number;
}