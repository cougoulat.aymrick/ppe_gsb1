import { Test, TestingModule } from '@nestjs/testing';
import { ProjetController } from './projet.controller';

describe('Projet Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [ProjetController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: ProjetController = module.get<ProjetController>(ProjetController);
    expect(controller).toBeDefined();
  });
});
