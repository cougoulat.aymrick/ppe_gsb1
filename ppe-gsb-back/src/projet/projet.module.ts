import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjetService } from './projet.service';
import { ProjetController } from './projet.controller';
import { Projet } from './projet.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Projet])],
  providers: [ProjetService],
  controllers: [ProjetController],
})
export class ProjetModule {}