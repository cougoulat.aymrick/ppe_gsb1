import { Controller, Get, All, Post, Body, Req, Inject, forwardRef, Param, Put, Delete } from '@nestjs/common';
import { ProjetService } from './projet.service';

@Controller('projet')
export class ProjetController {
    constructor(@Inject(forwardRef(() => ProjetService)) private projet: ProjetService) {}

    @Get()
    async findAll() {
      return this.projet.repo.find();
    }

    @Get(':name')
    findOne(@Param() params) {
        return this.projet.repo.find({ where: { nom: params.name } });
    }

    @Post()
    public create(@Body() body) {
        const request = this.projet.repo.save(this.projet.repo.merge(this.projet.repo.create(), body));
        return request;
    }

    @Put()
    public update(@Body() body) {
        return this.projet.repo.save(body);
    }

    @Delete(':id')
    public delete(@Param() params) {
        return this.projet.repo.delete(params.id);
    }
}
