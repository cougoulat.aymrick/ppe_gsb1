import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TacheService } from './tache.service';
import { TacheController } from './tache.controller';
import { Tache } from './tache.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Tache])],
  providers: [TacheService],
  controllers: [TacheController],
})
export class TacheModule {}