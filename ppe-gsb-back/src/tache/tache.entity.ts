import { Entity, Column, PrimaryGeneratedColumn, Generated,  } from 'typeorm';

@Entity()
export class Tache {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column({length: 100, nullable: true})
    libelle: string;
    
    @Column({length: 100, nullable: true})
    titre: string;
    
    @Column({type: "float", nullable: true})
    nb_jours: number;
    
    @Column({type: "longtext", nullable: true})
    description: string;    
    
    @Column({nullable: true})
    id_status: number;

    @Column({nullable: true})
    id_utilisateur: number;

    @Column({nullable: true})
    id_projet: number;
}