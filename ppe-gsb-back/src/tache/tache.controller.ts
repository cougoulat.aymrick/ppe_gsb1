import { Controller, Get, All, Post, Body, Req, Inject, forwardRef, Param, Put, Delete } from '@nestjs/common';
import { TacheService } from './tache.service';

@Controller('tache')
export class TacheController {
    constructor(@Inject(forwardRef(() => TacheService)) private tache: TacheService) {}

    @Get()
    async findAll() {
      return this.tache.repo.find();
    }

    @Get(':id')
    findOne(@Param() params) {
        const request = `SELECT * from tache WHERE id = '${params.id}'`;
        return this.tache.repo.query(request);
    }

    @Post()
    public create(@Body() body) {
        const request = this.tache.repo.save(this.tache.repo.merge(this.tache.repo.create(), body));
        return request;
    }

    @Put()
    // public update(@Body() body) {
    //     console.log(body);
    //     const request = `UPDATE tache SET
    //                         libelle = '${body.libelle}',
    //                         titre = '${body.titre}',
    //                         description = `${body.description}`,
    //                         id_status = '${body.id_status}',
    //                         id_utilisateur = '${body.id_utilisateur}',
    //                         id_projet = '${body.id_projet}',
    //                         nb_jours = '${body.nb_jours}'
    //                      WHERE id = '${body.id}'`;
    //     return this.tache.repo.query(request);
    @Put()
    public update(@Body() body) {
        return this.tache.repo.save(body);
    }

    @Delete(':id')
    public delete(@Param() params) {
        const request = `DELETE from tache WHERE id = '${params.id}'`;
        return this.tache.repo.query(request);
    }
}
