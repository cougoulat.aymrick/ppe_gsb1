import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Tache } from './tache.entity';

@Injectable()
export class TacheService {
  constructor(
    @InjectRepository(Tache)
    public readonly repo: Repository<Tache>,
  ) {}
}