import { Test, TestingModule } from '@nestjs/testing';
import { TacheController } from './tache.controller';

describe('Tache Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [TacheController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: TacheController = module.get<TacheController>(TacheController);
    expect(controller).toBeDefined();
  });
});
