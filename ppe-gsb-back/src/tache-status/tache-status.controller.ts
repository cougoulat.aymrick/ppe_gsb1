import { Controller, Get, Param, forwardRef, Inject, Post, Body, Delete } from '@nestjs/common';
import { TacheStatusService } from './tache-status.service';

@Controller('tache-status')
export class TacheStatusController {
    constructor(@Inject(forwardRef(() => TacheStatusService)) private tacheStatus: TacheStatusService) {}

    @Get()
    async findAll() {
      return this.tacheStatus.repo.find();
    }

    @Get(':id')
    findOne(@Param() params) {
        const request = `SELECT * from tache_status WHERE id = '${params.id}'`;
        return this.tacheStatus.repo.query(request);
    }
}
