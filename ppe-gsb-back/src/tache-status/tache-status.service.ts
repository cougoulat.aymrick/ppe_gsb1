import { Injectable} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TacheStatus } from './tache-status.entity';

@Injectable()
export class TacheStatusService {
  constructor(
    @InjectRepository(TacheStatus)
    public readonly repo: Repository<TacheStatus>,
  ) {}
}