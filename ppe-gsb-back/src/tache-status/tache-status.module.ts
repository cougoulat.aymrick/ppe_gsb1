import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TacheStatusService } from './tache-status.service';
import { TacheStatusController } from './tache-status.controller';
import { TacheStatus } from './tache-status.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TacheStatus])],
  providers: [TacheStatusService],
  controllers: [TacheStatusController],
})
export class TacheStatusModule {}