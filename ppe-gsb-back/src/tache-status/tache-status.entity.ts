import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class TacheStatus {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column({length: 100, nullable: true})
    libelle: string;
}