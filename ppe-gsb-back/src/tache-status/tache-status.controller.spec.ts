import { Test, TestingModule } from '@nestjs/testing';
import { TacheStatusController } from './tache-status.controller';

describe('TacheStatus Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [TacheStatusController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: TacheStatusController = module.get<TacheStatusController>(TacheStatusController);
    expect(controller).toBeDefined();
  });
});
