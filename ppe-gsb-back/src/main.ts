import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';
import * as session from 'express-session';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    methods: ['GET', 'PUT', 'POST', 'DELETE'],
    credentials: true,
    origin: true,
  });
  app.use(cookieParser('azerty'));
  await app.listen(3000);
}
bootstrap();
