import { Get, Controller, Req } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('api')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('toto')
  root(): string {
    return this.appService.root();
  }
}
