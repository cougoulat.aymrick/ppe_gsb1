import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Rang } from './rang.entity';

@Injectable()
export class RangService {
  constructor(
    @InjectRepository(Rang)
    public readonly repo: Repository<Rang>,
  ) {}
}