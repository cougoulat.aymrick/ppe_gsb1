import { Controller, Inject, forwardRef, Get } from '@nestjs/common';
import { RangService } from './rang.service';

@Controller('rang')
export class RangController {
    constructor(@Inject(forwardRef(() => RangService)) private rang: RangService) {}

    @Get()
    findAll() {
        const request = `SELECT * from rang_utilisateur`;
        return this.rang.repo.query(request);
    }
}
