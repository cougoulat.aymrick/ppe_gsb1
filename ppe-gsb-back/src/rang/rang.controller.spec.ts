import { Test, TestingModule } from '@nestjs/testing';
import { RangController } from './rang.controller';

describe('Rang Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [RangController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: RangController = module.get<RangController>(RangController);
    expect(controller).toBeDefined();
  });
});
