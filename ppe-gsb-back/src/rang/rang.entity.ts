import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Rang {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column({length: 100, nullable: true})
    code: string;
}