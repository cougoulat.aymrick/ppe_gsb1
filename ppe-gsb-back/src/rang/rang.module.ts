import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RangService } from './rang.service';
import { RangController } from './rang.controller';
import { Rang } from './rang.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Rang])],
  providers: [RangService],
  controllers: [RangController],
})
export class RangModule {}
