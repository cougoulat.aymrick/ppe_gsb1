
import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ProjetUtilisateur } from './projet-utilisateur.entity';

@Injectable()
export class ProjetUtilisateurService {
  constructor(
    @InjectRepository(ProjetUtilisateur)
    public readonly repo: Repository<ProjetUtilisateur>,
  ) {}
}