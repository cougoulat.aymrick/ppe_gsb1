import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ProjetUtilisateur {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    id_projet: number;

    @Column()
    id_utilisateur: number;
}