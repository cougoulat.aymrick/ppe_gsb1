import { Controller, Get, Param, forwardRef, Inject, Post, Body, Delete } from '@nestjs/common';
import { ProjetUtilisateurService } from './projet-utilisateur.service';
import { FindOperator } from 'typeorm';

@Controller('projet-utilisateur')
export class ProjetUtilisateurController {
    constructor(@Inject(forwardRef(() => ProjetUtilisateurService)) private projetUtilisateur: ProjetUtilisateurService) {}

    @Get()
    findAll() {
        const request = `SELECT * from projet_utilisateur`;
        return this.projetUtilisateur.repo.query(request);
    }

    @Get(':id_projet')
    findOne(@Param() params) {
        const request = `SELECT * from projet_utilisateur WHERE id_projet = '${params.id_projet}'`;
        return this.projetUtilisateur.repo.query(request);
    }

    @Get('utilisateur/:id_utilisateur')
    find(@Param() params) {
        const request = `SELECT * from projet_utilisateur WHERE id_utilisateur = '${params.id_utilisateur}'`;
        return this.projetUtilisateur.repo.query(request);
    }

    @Post()
    public create(@Body() body) {
        const request = this.projetUtilisateur.repo.save(this.projetUtilisateur.repo.merge(this.projetUtilisateur.repo.create(), body));
        return request;
    }

    @Delete(':id_projet')
    public delete(@Param() params) {
        const request = `DELETE from projet_utilisateur WHERE id = '${params.id_projet}'`;
        return this.projetUtilisateur.repo.query(request);
    }
}
