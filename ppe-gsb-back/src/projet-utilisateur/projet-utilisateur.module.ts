import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjetUtilisateurService } from './projet-utilisateur.service';
import { ProjetUtilisateurController } from './projet-utilisateur.controller';
import { ProjetUtilisateur } from './projet-utilisateur.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ProjetUtilisateur])],
  providers: [ProjetUtilisateurService],
  controllers: [ProjetUtilisateurController],
})
export class ProjetUtilisateurModule {}