import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UtilisateurModule } from './utilisateur/utilisateur.module';
import { RangModule } from './rang/rang.module';
import { ProjetModule } from './projet/projet.module';
import { ProjetUtilisateurModule } from './projet-utilisateur/projet-utilisateur.module';
import { TacheModule } from './tache/tache.module';
import { TacheStatusModule } from './tache-status/tache-status.module';
import { AuthModule } from './auth/auth.module';
import { UtilisateurService } from 'utilisateur/utilisateur.service';
import { ProjetStatusModule } from 'projet-status/projet-status.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'ppe_user',
      password: '123456',
      database: 'ppe_gdp',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    UtilisateurModule,
    RangModule,
    ProjetModule,
    ProjetUtilisateurModule,
    TacheModule,
    TacheStatusModule,
    AuthModule,
    ProjetStatusModule
  ],
  controllers: [AppController],
  providers: [AppService, UtilisateurService],
})
export class AppModule {
  constructor(private readonly connection: Connection) {}
}
