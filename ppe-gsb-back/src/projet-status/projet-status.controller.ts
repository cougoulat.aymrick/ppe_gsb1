import { Controller, Get, Param, forwardRef, Inject, Post, Body, Delete } from '@nestjs/common';
import { ProjetStatusService } from './projet-status.service';

@Controller('projet-status')
export class ProjetStatusController {
    constructor(@Inject(forwardRef(() => ProjetStatusService)) private projetStatus: ProjetStatusService) {}

    @Get()
    find(@Param() params) {
        const request = `SELECT * from projet_status`;
        return this.projetStatus.repo.query(request);
    }
}
