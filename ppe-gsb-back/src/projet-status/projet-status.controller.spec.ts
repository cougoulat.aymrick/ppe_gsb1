import { Test, TestingModule } from '@nestjs/testing';
import { ProjetStatusController } from './projet-status.controller';

describe('ProjetStatus Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [ProjetStatusController ],
    }).compile();
  });
  it('should be defined', () => {
    const controller: ProjetStatusController  = module.get<ProjetStatusController >(ProjetStatusController );
    expect(controller).toBeDefined();
  });
});
