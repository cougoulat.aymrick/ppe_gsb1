import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ProjetStatus {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column({length: 100, nullable: true})
    code: string;
}