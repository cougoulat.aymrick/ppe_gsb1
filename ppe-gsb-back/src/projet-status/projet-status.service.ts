import { Injectable} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ProjetStatus } from './projet-status.entity';

@Injectable()
export class ProjetStatusService {
  constructor(
    @InjectRepository(ProjetStatus)
    public readonly repo: Repository<ProjetStatus>,
  ) {}
}