import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjetStatusService } from './projet-status.service';
import { ProjetStatusController } from './projet-status.controller';
import { ProjetStatus } from './projet-status.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ProjetStatus])],
  providers: [ProjetStatusService],
  controllers: [ProjetStatusController],
})
export class ProjetStatusModule {}