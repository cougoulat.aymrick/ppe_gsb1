
import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Utilisateur } from './utilisateur.entity';
import * as crypto from 'crypto-js';

@Injectable()
export class UtilisateurService {
  constructor(
    @InjectRepository(Utilisateur)
    public readonly repo: Repository<Utilisateur>,
  ) {}
}