import { Entity, Column, PrimaryGeneratedColumn, Generated,  } from 'typeorm';

@Entity()
export class Utilisateur {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    @Generated('uuid')
    uid: string;
    
    @Column({length: 100, nullable: true})
    prenom: string;
    
    @Column({length: 100, nullable: true})
    nom: string;
    
    @Column({length: 100, nullable: true})
    prenom_dieme: string;
    
    @Column({length: 100, nullable: true})
    prenom_tieme: string;    
    
    @Column("date", {nullable: true})
    date_naissance;
    
    @Column({length: 100, nullable: true})
    ville_naissance: string;
    
    @Column({length: 100, nullable: true})
    adresse: string;
    
    @Column({length: 100, nullable: true})
    cp: string;
    
    @Column({length: 100, nullable: true})
    ville: string;
    
    @Column({length: 100, nullable: true})
    nom_emploi: string;

    @Column({nullable: true})
    debut_horaire: string;
    
    @Column({nullable: true})
    fin_horaire: string;

    @Column({nullable: true})
    id_rang: number;

    @Column({length: 100, nullable: true})
    identifiant: string;

    @Column({length: 512, nullable: true})
    password: string;
}