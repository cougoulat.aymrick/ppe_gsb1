import { Controller, Get, All, Post, Body, Req, Inject, forwardRef, Param, Put, Delete } from '@nestjs/common';
import { UtilisateurService } from './utilisateur.service'
import { In, Like } from 'typeorm';

@Controller('utilisateur')
export class UtilisateurController {
    constructor(@Inject(forwardRef(() => UtilisateurService)) private utilisateur: UtilisateurService) {}

    @Get()
    async findAll() {
      return this.utilisateur.repo.find();
    }

    @Get(':uid')
    findOne(@Param() params) {
        const request = `SELECT * from utilisateur WHERE uid = '${params.uid}'`;
        return this.utilisateur.repo.query(request);
    }

    @Post()
    public create(@Body() body) {
        const request = this.utilisateur.repo.save(this.utilisateur.repo.merge(this.utilisateur.repo.create(), body));
        return request;
    }

    @Put()
    public update(@Body() body) {
        console.log(body);
        const request = `UPDATE utilisateur SET 
                            prenom = '${body.prenom}',
                            nom = '${body.nom}',
                            prenom_dieme = '${body.prenom_dieme}',
                            prenom_tieme = '${body.prenom_tieme}',
                            ville_naissance = '${body.ville_naissance}',
                            date_naissance = '${body.date_naissance}',
                            adresse = '${body.adresse}',
                            ville = '${body.ville}',
                            cp = '${body.cp}',
                            nom_emploi = '${body.nom_emploi}',
                            debut_horaire = '${body.debut_horaire}',
                            fin_horaire = '${body.fin_horaire}',
                            id_rang = ${body.id_rang}
                         WHERE uid = '${body.uid}'`;
        return this.utilisateur.repo.query(request);
    }

    @Delete(':id')
    public delete(@Param() params) {
        const request = `DELETE from utilisateur WHERE id = '${params.id}'`;
        return this.utilisateur.repo.query(request);
    }
}
