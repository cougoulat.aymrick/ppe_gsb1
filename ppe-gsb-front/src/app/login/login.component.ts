import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private authService: AuthService, 
              private router: Router) { }

  auth : any = {
    identifiant: null,
    password: null
  };

  loginUser(auth) {
    this.authService.getUserDetails(auth).subscribe(res => {
      if (res) {
        sessionStorage.setItem("token", res.toString());
        this.router.navigate(['/accueil']);
      }
    });
  }

}