import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClarityModule } from '@clr/angular';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { UtilisateurModule } from './utilisateur/utilisateur.module';
import { ProjetModule } from './projet/projet.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { InterfaceComponent } from './interface/interface.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    InterfaceComponent,
    LoginComponent,
    HomeComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ClarityModule,
    AppRoutingModule,
    UtilisateurModule,
    ProjetModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
