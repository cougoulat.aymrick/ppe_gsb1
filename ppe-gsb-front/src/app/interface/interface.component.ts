import { Component } from '@angular/core';
import { AuthService } from '../service/auth/auth.service';
import { Router } from '@angular/router';
import { UtilisateurService } from '../service/utilisateur/utilisateur.service';

@Component({
  selector: 'app-interface',
  templateUrl: './interface.component.html',
  styleUrls: ['./interface.component.css']
})
export class InterfaceComponent {
  collapsed: any = null;

  constructor(private AuthService: AuthService,
              private router: Router,
              private utilisateurService: UtilisateurService,
              )
  {
  }

  public title = 'Gestionnaire de projet - GSB';
  public b_admin: boolean = false;
  public user: Object = {};
  public listRang: Array<any> = [];
  public session = { token: sessionStorage.getItem("token") };
  public resultRequest : Promise<boolean> = Promise.resolve(false);

  async ngOnInit() {
    if (!this.session.token) {
      this.router.navigate(["/connexion"]);
    }
    await this.AuthService.validateToken(this.session).subscribe(res => {
      this.user = res;
      if (!this.user) {
        this.router.navigate(["/connexion"]);
      }
      this.utilisateurService.getListRang().subscribe(res => {
        this.listRang = res as any[];
        const statusUser = this.listRang.find(el => parseInt(el.id, 10) === parseInt(this.user["id_rang"], 10));
        statusUser["code"] === "ADMIN" ? this.b_admin = true : this.b_admin = false;
      });
      this.resultRequest = Promise.resolve(true);
    });
  }

  disconnect() {
    sessionStorage.clear();
    this.router.navigate(["/connexion"]);
  }
}
