import { Component, OnInit } from '@angular/core';
import { ProjetService } from '../../service/projet/projet.service';
import { UtilisateurService } from '../../service/utilisateur/utilisateur.service';
import { AuthService } from '../../service/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-projet',
  templateUrl: './list-projet.component.html',
  styleUrls: ['./list-projet.component.css']
})
export class ListProjetComponent implements OnInit {

  public projets;
  public utilisateurs;
  public statuts;
  public user: Object = {};
  public listRang: Array<any> = [];
  public session = { token: sessionStorage.getItem("token") };
  public b_admin : boolean = false;
  
  constructor(private projetService: ProjetService,
              private utilisateurService: UtilisateurService,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.authService.validateToken(this.session).subscribe(res => {
      this.user = res;
      if (!this.user) {
        this.router.navigate(["/connexion"]);
      }
      this.utilisateurService.getListRang().subscribe(res => {
        this.listRang = res as any[];
        const statusUser = this.listRang.find(el => parseInt(el.id, 10) === parseInt(this.user["id_rang"], 10));
        statusUser["code"] === "ADMIN" ? this.b_admin = true : this.b_admin = false;
      });
      this.projetService.getListProjet().subscribe(res =>{
        this.projets = res;
        this.projetService.getListProjetUtilisateur().subscribe(res=>{
          if (!this.b_admin) {
            const listProjetFiltered = [];
            let projetUtilisateur = (res as any[]);
            console.log(projetUtilisateur);
            projetUtilisateur = projetUtilisateur.filter(el => el.id_utilisateur == this.user["id"]);
            console.log(projetUtilisateur);
            projetUtilisateur.forEach(element => {
              const projet = this.projets.find(el => el.id == element.id_projet);
              listProjetFiltered.push(projet);
            });
            this.projets = listProjetFiltered;
          }
        });
      });
      this.utilisateurService.getListUtilisateur().subscribe(res => this.utilisateurs = res);
      this.projetService.getListProjetStatus().subscribe(res => this.statuts = res);
    });
  }

  getChefProjet(projet, utilisateurs) {
    if (projet && utilisateurs) {
      const utilisateurChef = utilisateurs.find(el => el.id == projet.id_chef_projet_utilisateur);
      if (utilisateurChef) return utilisateurChef.nom + " " + utilisateurChef.prenom;
    }
    return "Personne";
  }

  getStatut(projet, statuts) {
    if (projet && statuts) {
      const statusProjet = statuts.find(el => parseInt(el.id, 10) == parseInt(projet.id_status, 10));
      return statusProjet.code;
    }
  }

}
