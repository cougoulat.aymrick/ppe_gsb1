import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';

import { FicheProjetComponent } from './fiche-projet.component';
import { ProjetDescriptionComponent } from './projet-description/projet-description.component';
import { ProjetTacheComponent } from './projet-tache/projet-tache.component';
import { ProjetUtilisateurComponent } from './projet-utilisateur/projet-utilisateur.component';

@NgModule({
  declarations: [FicheProjetComponent, ProjetDescriptionComponent, ProjetTacheComponent, ProjetUtilisateurComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ClarityModule
  ],
  exports: [FicheProjetComponent, ProjetTacheComponent, ProjetUtilisateurComponent, ProjetDescriptionComponent]
})
export class FicheProjetModule { }
