import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-projet-description',
  templateUrl: './projet-description.component.html',
  styleUrls: ['./projet-description.component.css']
})
export class ProjetDescriptionComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }
  
  @Input('projet') projet;
  @Input('utilisateurs') utilisateurs;
  @Input('listProjetStatus') listProjetStatus;
  @Input('b_admin') b_admin: boolean;
  public name: String;

  ngOnInit() {
    this.name = this.route.snapshot.paramMap.get('name');
  }
}

