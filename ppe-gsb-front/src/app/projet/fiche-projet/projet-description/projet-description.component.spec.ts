import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjetDescriptionComponent } from './projet-description.component';

describe('ProjetDescriptionComponent', () => {
  let component: ProjetDescriptionComponent;
  let fixture: ComponentFixture<ProjetDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjetDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjetDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
