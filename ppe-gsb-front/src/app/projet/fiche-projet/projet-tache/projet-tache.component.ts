import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClrWizard } from "@clr/angular";
import { TacheService } from '../../../service/tache/tache.service';
import { Tache } from '../../../service/class/Tache';

@Component({
  selector: 'app-projet-tache',
  templateUrl: './projet-tache.component.html',
  styleUrls: ['./projet-tache.component.css']
})
export class ProjetTacheComponent implements OnInit {

  name: String;
  @ViewChild("wizardmg") wizardMedium: ClrWizard;
  popupTache: boolean = false;
  tache: Tache = new Tache();
  tachesProjet = [];
  typeSave: String = '';

  @Input("utilisateurs") utilisateurs;
  @Input("projet") projet;
  @Input("listTache") listTache;
  @Input("listTacheStatus") listTacheStatus;
  @Input('b_admin') b_admin: boolean;

  constructor(private route: ActivatedRoute,
              private tacheService: TacheService) { }

  ngOnInit() {
    this.name = this.route.snapshot.paramMap.get('name');
    this.tachesProjet = this.listTache.filter(el => parseInt(el.id_projet, 10) == parseInt(this.projet.id, 10));
  }

  getStatus(id_status) {
    const status = this.listTacheStatus.find(el => el.id == id_status);
    return status.libelle;
  }

  getUtilisateur(id_utilisateur) {
    const utilisateur = this.utilisateurs.find(el => el.id == id_utilisateur);
    return utilisateur.nom + " " + utilisateur.prenom;
  }

  addPopupTache() {
    this.tache = {};
    this.typeSave = 'add';
    this.popupTache = true;
  }

  deletePopupTache(tache) {
    this.typeSave = 'delete';
    this.saveTache(tache, this.typeSave);
  }

  editPopupTache(tache) {
    this.tache = {
      id: tache.id,
      libelle: tache.libelle,
      titre: tache.titre,
      id_status: tache.id_status,
      id_utilisateur: tache.id_utilisateur,
      id_projet: tache.id_projet,
      nb_jours: tache.nb_jours,
      description: tache.description
    };
    this.typeSave = 'edit';
    this.popupTache = true;
  }

  saveTache(tache, type) {
    switch (type) {
      case 'delete':
        this.tachesProjet = this.tachesProjet.filter(el => el.id != tache.id);
        this.tacheService.deleteTache(tache.id);
        break;
      case 'add' :
        tache.id_projet = this.projet.id;
        this.tachesProjet.push(tache);
        this.tacheService.saveTache(tache);
        break;
      case 'edit':
        this.tacheService.updateTache(tache);
        break;
    }
  }

}

