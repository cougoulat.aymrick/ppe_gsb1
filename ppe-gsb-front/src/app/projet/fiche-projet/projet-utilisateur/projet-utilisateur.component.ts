import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-projet-utilisateur',
  templateUrl: './projet-utilisateur.component.html',
  styleUrls: ['./projet-utilisateur.component.css']
})
export class ProjetUtilisateurComponent implements OnInit {

  name: String;
  newUser: Object;
  deleteUser: Object;
  utilisateursSansProjet;
  @Input('projet') projet;
  @Input('utilisateurs') utilisateurs;
  @Input('utilisateursProjet') utilisateursProjet;
  @Input('b_admin') b_admin: boolean;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.name = this.route.snapshot.paramMap.get('name');
    this.utilisateursSansProjet = this.utilisateurs;
    if (this.utilisateursProjet) {
      this.utilisateursProjet.forEach(element => {
        const utilisateurDatatemp = this.utilisateurs.find(el => parseInt(element.id_utilisateur, 10) === el.id);
        this.utilisateursSansProjet = this.utilisateursSansProjet.filter(el => el.id !== parseInt(element.id_utilisateur, 10));
        element.nom = utilisateurDatatemp.nom;
        element.prenom = utilisateurDatatemp.prenom;
        element.date_naissance = utilisateurDatatemp.date_naissance;
      });
    }
  }

  getChefProjet(utilisateur) {
    return (utilisateur.id_utilisateur == this.projet.id_chef_projet_utilisateur) ? "OUI" : "NON";
  }

  addUtilisateur(id_utilisateur) {
    if (this.utilisateursProjet == undefined) {
      this.utilisateursProjet = [];
    }
    const utilisateurData = this.utilisateurs.find(el => parseInt(id_utilisateur, 10) === el.id);
    if (!this.utilisateursProjet.find(el => el.id_utilisateur === parseInt(id_utilisateur, 10))) {
      this.newUser = {
        id_projet : this.projet.id,
        id_utilisateur : parseInt(utilisateurData.id, 10),
        nom: utilisateurData.nom,
        prenom: utilisateurData.prenom,
        date_naissance: utilisateurData.date_naissance,
        new: true
      }
      this.utilisateursSansProjet = this.utilisateursSansProjet.filter(el => el.id !== parseInt(id_utilisateur, 10));
      this.utilisateursProjet.push(this.newUser);
    }
  }

  deleteUtilisateur(id_utilisateur) {
    const utilisateurProjet = this.utilisateursProjet.find(el => parseInt(id_utilisateur, 10) === el.id_utilisateur);
    this.utilisateursProjet = this.utilisateursProjet.filter(el => el.id_utilisateur !== parseInt(utilisateurProjet.id_utilisateur, 10));
    if (utilisateurProjet.id) {
      utilisateurProjet.delete = true;
      this.utilisateursSansProjet.push(utilisateurProjet);
      this.utilisateursProjet.push(this.deleteUser);
    } else {
      this.utilisateursSansProjet.push(utilisateurProjet);
    }
  }

}
