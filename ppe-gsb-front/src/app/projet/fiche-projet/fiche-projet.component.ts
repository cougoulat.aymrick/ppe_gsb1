import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjetService } from '../../service/projet/projet.service';
import { UtilisateurService } from '../../service/utilisateur/utilisateur.service';
import { TacheService } from '../../service/tache/tache.service';
import { Location } from '@angular/common';
import * as moment from 'moment';
import { AuthService } from '../../service/auth/auth.service';

import { User } from '../../service/class/User';
import { Projet } from '../../service/class/Projet';

@Component({
  selector: 'app-fiche-projet',
  templateUrl: './fiche-projet.component.html',
  styleUrls: ['./fiche-projet.component.css']
})
export class FicheProjetComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private projetService: ProjetService,
    private utilisateurService: UtilisateurService,
    private tacheService: TacheService,
    private authService: AuthService,
    private router: Router,
    private location: Location) { }

  public menu: string;
  public addProjet: boolean = false;
  public editProjet: boolean = false;
  public projet: Projet = {
    nom: "",
    createur: "",
    client: "",
    date_debut: null,
    date_fin: null,
    description: "",
    id_chef_projet_utilisateur: null,
    id_status: null
  };
  public user: User;
  public utilisateurs;
  public utilisateursProjet;
  public utilisateursProjetOrigin;
  public listTacheStatus;
  public listTache;
  public listProjetStatus;
  public listRang : Array<any> = [];
  public b_admin : boolean = false;
  public session = { token: sessionStorage.getItem("token")};

  ngOnInit() {
    const name = this.route.snapshot.paramMap.get('name');
    if (name === null) {
      this.addProjet = true;
    } else {
      this.editProjet = true;
      this.projetService.getProjetByName(name).subscribe(res => {
        res[0].date_debut = moment(res[0].date_debut).format('MM/DD/YYYY');
        res[0].date_fin = moment(res[0].date_fin).format('MM/DD/YYYY');
        this.projet = res[0]
        this.projetService.getListUtilisateurProjet(res[0].id).subscribe(res => this.utilisateursProjet = res);
      });
    }
    this.authService.validateToken(this.session).subscribe(res => {
      this.user = res;
      if (!this.user) {
        this.router.navigate(["/connexion"]);
      }
      this.utilisateurService.getListRang().subscribe(res => {
        this.listRang = res as any[];
        const statusUser = this.listRang.find(el => el.id == this.user["id_rang"]);
        statusUser["code"] == "ADMIN" ? this.b_admin = true : this.b_admin = false;
      });
    });
    this.utilisateurService.getListUtilisateur().subscribe(res => this.utilisateurs = res);
    this.tacheService.getListTacheStatus().subscribe(res => this.listTacheStatus = res);
    this.tacheService.getListTache().subscribe(res => this.listTache = res);
    this.projetService.getListProjetStatus().subscribe(res => this.listProjetStatus = res);
    this.menu = 'description';
  }

  save(projet, utilisateursProjet) {
    projet.date_debut = moment(projet.date_debut).format('YYYY-MM-DD');
    projet.date_fin = moment(projet.date_fin).format('YYYY-MM-DD');
    projet.id_status = parseInt(projet.id_status, 10);
    if (this.addProjet) {
      this.projetService.saveProjet(projet);
    } else if (this.editProjet) {
      this.projetService.updateProjet(projet);
    }
    if (utilisateursProjet) {
      utilisateursProjet.forEach( element => {
        if (element.new && !element.delete) {
          this.projetService.saveProjetUtilisateur(element);
        } else if (element.delete) {
          this.projetService.deleteProjetUtilisateur(element.id);
        }
      });
    }
    this.location.back();
  }

  delete(projet) {
    this.projetService.deleteProjet(projet.id);
    this.location.back();
  }

  goBack() {
    this.location.back();
  }
}
