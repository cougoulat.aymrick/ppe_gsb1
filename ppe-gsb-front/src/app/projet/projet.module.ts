import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ClarityModule } from '@clr/angular';

import { ListProjetComponent } from './list-projet/list-projet.component';
import { FicheProjetModule } from './fiche-projet/fiche-projet.module';

@NgModule({
  declarations: [ListProjetComponent],
  imports: [
    CommonModule,
    RouterModule,
    ClarityModule,
    FicheProjetModule
  ],
  exports: [ListProjetComponent, FicheProjetModule]
})
export class ProjetModule { }
