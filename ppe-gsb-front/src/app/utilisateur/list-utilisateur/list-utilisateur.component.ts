import { Component, OnInit } from '@angular/core';
import { UtilisateurService } from '../../service/utilisateur/utilisateur.service';
import { AuthService } from '../../service/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-utilisateur',
  templateUrl: './list-utilisateur.component.html',
  styleUrls: ['./list-utilisateur.component.css']
})
export class ListUtilisateurComponent implements OnInit {

  constructor(private utilisateurService: UtilisateurService,
              private authService: AuthService,
              private router : Router) { }

  public utilisateurs: Array<any> = [];
  public user: Object = {};
  public listRang: Array<any> = [];
  public session = { token: sessionStorage.getItem("token") };
  public b_admin : boolean = false;

  ngOnInit() {
    this.utilisateurService.getListUtilisateur().subscribe(res => this.utilisateurs = res as any[]);
    this.authService.validateToken(this.session).subscribe(res => {
      this.user = res;
      if (!this.user) {
        this.router.navigate(["/connexion"]);
      }
      this.utilisateurService.getListRang().subscribe(res => {
        this.listRang = res as any[];
        const statusUser = this.listRang.find(el => el.id == this.user["id_rang"]);
        statusUser["code"] == "ADMIN" ? this.b_admin = true : this.b_admin = false;
        if (this.b_admin == false) {
          this.router.navigate(["/connexion"]);
        }
      });
    });
  }

}
