import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-utilisateur-description, ClrSelect',
  templateUrl: './utilisateur-description.component.html',
  styleUrls: ['./utilisateur-description.component.css']
})
export class UtilisateurDescriptionComponent implements OnInit {

  @Input('user') user;
  @Input('listRangUtilisateur') listRang: Array<any>;
  @Input('b_admin') b_admin : boolean;
  uid : String = "";

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.uid = this.route.snapshot.paramMap.get('uid');
  }
}

