import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilisateurDescriptionComponent } from './utilisateur-description.component';

describe('UtilisateurDescriptionComponent', () => {
  let component: UtilisateurDescriptionComponent;
  let fixture: ComponentFixture<UtilisateurDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtilisateurDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UtilisateurDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
