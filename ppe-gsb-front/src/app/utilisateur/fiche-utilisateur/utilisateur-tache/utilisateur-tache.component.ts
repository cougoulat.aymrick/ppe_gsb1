import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-utilisateur-tache',
  templateUrl: './utilisateur-tache.component.html',
  styleUrls: ['./utilisateur-tache.component.css']
})
export class UtilisateurTacheComponent implements OnInit {

  constructor() { }

  @Input('taches') taches;
  @Input('user') user;
  @Input('tacheStatuts') tacheStatuts;
  public utilisateursTache;

  ngOnInit() {
    this.utilisateursTache = this.taches.filter(el => el.id_utilisateur === this.user.id);
    this.utilisateursTache.forEach(element => {
      element.status = (this.tacheStatuts.find(el => el.id === element.id_status)).libelle;      
    });
  }

}
