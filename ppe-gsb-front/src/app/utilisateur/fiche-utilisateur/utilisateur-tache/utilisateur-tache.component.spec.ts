import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilisateurTacheComponent } from './utilisateur-tache.component';

describe('UtilisateurTacheComponent', () => {
  let component: UtilisateurTacheComponent;
  let fixture: ComponentFixture<UtilisateurTacheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtilisateurTacheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UtilisateurTacheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
