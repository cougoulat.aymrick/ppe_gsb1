import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-utilisateur-projet',
  templateUrl: './utilisateur-projet.component.html',
  styleUrls: ['./utilisateur-projet.component.css']
})
export class UtilisateurProjetComponent implements OnInit {

  @Input('utilisateursProjet') utilisateursProjet : Array<any>;
  @Input('projets') projets : Array<any>;
  @Input('user') user : Object;
  public uid: String = "";

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.uid = this.route.snapshot.paramMap.get('uid');
    if (this.utilisateursProjet) {
      this.utilisateursProjet.forEach(element => {
        const projetData = this.projets.find(el => parseInt(element.id_projet, 10) === el.id);
        element.nom = projetData.nom;
        element.createur = projetData.createur;
        element.client = projetData.client;
        element.id_chef_projet_utilisateur = projetData.id_chef_projet_utilisateur;
      });
    }
  }
}
