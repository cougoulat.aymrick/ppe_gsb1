import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilisateurProjetComponent } from './utilisateur-projet.component';

describe('UtilisateurProjetComponent', () => {
  let component: UtilisateurProjetComponent;
  let fixture: ComponentFixture<UtilisateurProjetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtilisateurProjetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UtilisateurProjetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
