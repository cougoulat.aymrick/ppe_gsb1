import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { UtilisateurService } from '../../service/utilisateur/utilisateur.service';
import { ProjetService } from '../../service/projet/projet.service';
import { TacheService } from '../../service/tache/tache.service';
import { AuthService } from '../../service/auth/auth.service';

import * as moment from 'moment';
import { User } from '../../service/class/User';

@Component({
  selector: 'app-fiche-utilisateur',
  templateUrl: './fiche-utilisateur.component.html',
  styleUrls: ['./fiche-utilisateur.component.css']
})
export class FicheUtilisateurComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private utilisateurService: UtilisateurService,
              private projetService : ProjetService,
              private tacheService : TacheService,
              private location: Location,
              private authService: AuthService,
              private router : Router) { }

  public menu: string = 'description';
  public addUser: boolean = false;
  public editUser: boolean = false;
  public user: User = {
    nom: "",
    prenom: "",
    prenom_dieme: "",
    prenom_tieme: "",
    ville_naissance: "",
    date_naissance: null,
    adresse: "",
    cp: "",
    ville: "",
    nom_emploi: "",
    id_rang: null,
    debut_horaire: "",
    fin_horaire: "",
  };
  public listRang: Array<any> = [];
  public listRangUtilisateur: Array<any> = [];
  public utilisateursProjet: Array<any> = [];
  public userConnected: Object = {};
  public session = { token: sessionStorage.getItem("token") };
  public b_admin : boolean = false;
  public projets: Array<any> = [];
  public taches: Array<any> = [];
  public utilisateursTache: Array<any> = [];
  public tacheStatuts: Array<any> = [];

  ngOnInit() {
    const uid = this.route.snapshot.paramMap.get('uid');
    if (uid === null) {
      this.addUser = true;
    } else {
      this.editUser = true;
      this.utilisateurService.getUtilisateurByUid(uid).subscribe(res => {
        this.user = res[0];
        // this.user["date_naissance"] = moment(this.user["date_naissance"]).format('DD/MM/YYYY');
        this.utilisateurService.getListProjetByUtilisateur(this.user["id"]).subscribe(res => this.utilisateursProjet = res as any[]);
        this.authService.validateToken(this.session).subscribe(res => {
          this.userConnected = res;
          if (!this.userConnected) {
            this.router.navigate(["/connexion"]);
          }
          this.utilisateurService.getListRang().subscribe(res => {
            this.listRang = res as any[];
            const statusUser = this.listRang.find(el => el.id == this.userConnected["id_rang"]);
            statusUser["code"] == "ADMIN" ? this.b_admin = true : this.b_admin = false;
            if (this.b_admin == false && this.user["id"] != this.userConnected["id"]) {
              this.router.navigate(["/connexion"]);
            }
          });
        });
      });
    }
    this.utilisateurService.getListRang().subscribe(res => this.listRangUtilisateur = res as any[]);
    this.projetService.getListProjet().subscribe(res => this.projets = res as any[]);
    this.tacheService.getListTache().subscribe(res => this.taches = res as any[]);
    this.tacheService.getListTacheStatus().subscribe(res => this.tacheStatuts = res as any[]);
  }

  save(user) {
    user.date_naissance = moment(user.date_naissance).format('YYYY-MM-DD');
    if (this.addUser) {
      this.utilisateurService.saveUtilisateur(user);
    } else if (this.editUser) {
      this.utilisateurService.updateUtilisateur(user);
    }
    this.location.back();
  }

  delete(user) {
    this.utilisateurService.deleteUtilisateur(user.id);
    this.location.back();
  }

  goBack() {
    this.location.back();
  }
}