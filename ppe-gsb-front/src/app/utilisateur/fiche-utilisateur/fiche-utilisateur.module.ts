import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FicheUtilisateurComponent } from './fiche-utilisateur.component';
import { ClarityModule } from '@clr/angular';
import { UtilisateurDescriptionComponent } from './utilisateur-description/utilisateur-description.component';
import { UtilisateurProjetComponent } from './utilisateur-projet/utilisateur-projet.component';
import { UtilisateurTacheComponent } from './utilisateur-tache/utilisateur-tache.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [FicheUtilisateurComponent, UtilisateurDescriptionComponent, UtilisateurProjetComponent, UtilisateurTacheComponent],
  imports: [
    CommonModule,
    ClarityModule,
    FormsModule
  ]
})
export class FicheUtilisateurModule { }
