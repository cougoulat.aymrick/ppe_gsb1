import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FicheUtilisateurModule } from './fiche-utilisateur/fiche-utilisateur.module';
import { ClarityModule } from '@clr/angular';
import { RouterModule } from '@angular/router';

import { ListUtilisateurComponent } from './list-utilisateur/list-utilisateur.component';

@NgModule({
  declarations: [ListUtilisateurComponent],
  imports: [
    CommonModule,
    ClarityModule,
    RouterModule
  ],
  exports: [ListUtilisateurComponent, FicheUtilisateurModule]
})
export class UtilisateurModule { }
