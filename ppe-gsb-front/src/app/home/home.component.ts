import { Component, OnInit } from '@angular/core';
import { UtilisateurService } from '../service/utilisateur/utilisateur.service';
import { ProjetService } from '../service/projet/projet.service';
import { TacheService } from '../service/tache/tache.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private utilisateurService: UtilisateurService,
              private projetService: ProjetService,
              private tacheService: TacheService) { }

  public utilisateurs: Array<any> = [];
  public projets: Array<any> = [];
  public taches: Array<any> = [];
  public listRang: Array<any> = [];
  public tacheStatuts: Array<any> = [];
  public listProjetStatus: Array<any> = [];
  public dataProgress : number = 25;

  async ngOnInit() {
    await this.utilisateurService.getListUtilisateur().subscribe(res => this.utilisateurs = res as any[]);
    await this.utilisateurService.getListRang().subscribe(res => this.listRang = res as any[]);
    await this.projetService.getListProjet().subscribe(res => this.projets = res as any[]);
    await this.projetService.getListProjetStatus().subscribe(res => this.listProjetStatus = res as any[]);
    await this.tacheService.getListTache().subscribe(res => this.taches = res as any[]);
    await this.tacheService.getListTacheStatus().subscribe(res => this.tacheStatuts = res as any[]);
  }

  getData(activite, listStatuts, type) {
    if (activite.length > 0 && listStatuts.length > 0) {
      const statut = listStatuts.find(el => el[(el.libelle ? "libelle" : "code")] === type);
      const listActivites = activite.filter(el => parseInt(el[el.id_status ? "id_status" : "id_rang"], 10) == parseInt(statut["id"], 10));
      return listActivites.length;
    }
  }

  getTauxActivite(utilisateurs, listActicvite) {
    if (utilisateurs.length > 0 && listActicvite.length > 0) {
      const listActicviteFiltered = [];
      utilisateurs.forEach(element => {
        listActicviteFiltered.push(listActicvite.find(el => el.id_utilisateur != element.id));
      });
      const result = (100*(listActicviteFiltered.length + listActicvite.length - listActicvite.length))/(listActicviteFiltered.length + listActicvite.length);
      return result;
    }
  }

  getRound(value) {
    return Math.round(value);
  }
}