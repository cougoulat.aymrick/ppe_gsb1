import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  constructor(private http: HttpClient) { }

  url = environment.url;

  getListUtilisateur() {
    return this.http.get(`${this.url}/utilisateur`);
  }

  getUtilisateurByUid(uid) {
    return this.http.get(`${this.url}/utilisateur/${uid}`);
  }

  saveUtilisateur(user) {
    const request = this.http.post(`${this.url}/utilisateur`, user).
      subscribe(res => console.log("res %O", res),
                err => console.log("Error occured %O", err));
    return request;
  }

  updateUtilisateur(user) {
    const request =  this.http.put(`${this.url}/utilisateur`, user).
      subscribe(res => console.log("res", res),
                err => console.log("Error occured ", err));
    return request;
  }

  deleteUtilisateur(id) {
    const request = this.http.delete(`${this.url}/utilisateur/${id}`).
      subscribe(res => console.log("res %O", res),
                err => console.log("Error occured %O", err));
    return request;
  }

  getListRang() {
    const request = this.http.get(`${this.url}/rang`);
    return request;
  }

  getListProjetByUtilisateur(id_utilisateur) {
    const request = this.http.get(`${this.url}/projet-utilisateur/utilisateur/${id_utilisateur}`);
    return request;
  }
}
