import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjetService {

  constructor(private http: HttpClient) { }

  url = environment.url;

  getListProjet() {
    return this.http.get(`${this.url}/projet`);
  }

  getProjetByName(name) {
    return this.http.get(`${this.url}/projet/${name}`);
  }

  saveProjet(projet) {
    const request = this.http.post(`${this.url}/projet`, projet).
      subscribe(res => console.log("res %O", res),
                err => console.log("Error occured %O", err));
    return request;
  }

  updateProjet(projet) {
    const request =  this.http.put(`${this.url}/projet`, projet).
      subscribe(res => console.log("res", res),
                err => console.log("Error occured ", err));
    return request;
  }

  deleteProjet(id) {
    const request = this.http.delete(`${this.url}/projet/${id}`).
      subscribe(res => console.log("res %O", res),
                err => console.log("Error occured %O", err));
    return request;
  }

  getListProjetStatus() {
    const request = this.http.get(`${this.url}/projet-status`);
    return request;
  }

  getListProjetUtilisateur() {
    const request = this.http.get(`${this.url}/projet-utilisateur/`);
    return request;
  }

  getListUtilisateurProjet(id_projet) {
    const request = this.http.get(`${this.url}/projet-utilisateur/${id_projet}`);
    return request;
  }

  saveProjetUtilisateur(body) {
    const request = this.http.post(`${this.url}/projet-utilisateur`, body).
    subscribe(res => console.log("res %O", res),
              err => console.log("Error occured %O", err));
    return request;
  }

  deleteProjetUtilisateur(id_utilisateur) {
    const request = this.http.delete(`${this.url}/projet-utilisateur/${id_utilisateur}`).
      subscribe(res => console.log("res %O", res),
                err => console.log("Error occured %O", err));
    return request;
  }
}
