export class Tache {
    public id?: number
    public libelle?: string
    public titre?: string
    public nb_jours?: number
    public description?: string
    public id_utilisateur?: number
    public id_status?: number
    public id_projet?: number
}