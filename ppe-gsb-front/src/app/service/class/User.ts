export class User {
    public id?: number;
    public uid?: string
    public nom?: string
    public prenom?: string
    public prenom_dieme?: string
    public prenom_tieme?: string
    public ville_naissance?: string
    public date_naissance?: Date
    public adresse?: string
    public cp?: string
    public ville?: string
    public nom_emploi?: string
    public id_rang?: number
    public debut_horaire?: string
    public fin_horaire?: string
    public password?: string
    public identifiant?: string
}