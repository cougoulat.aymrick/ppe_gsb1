export class Projet {
    public id?: number
    public nom?: string
    public createur?: string
    public client?: string
    public date_debut?: Date
    public date_fin?: Date
    public description?: string
    public id_chef_projet_utilisateur?: number
    public id_status?: number
}