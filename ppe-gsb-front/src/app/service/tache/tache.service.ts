import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TacheService {

  constructor(private http: HttpClient) { }

  url = environment.url;

  getListTache() {
    return this.http.get(`${this.url}/tache`);
  }

  getTacheByName(id) {
    return this.http.get(`${this.url}/tache/${id}`);
  }

  saveTache(tache) {
    const request = this.http.post(`${this.url}/tache`, tache).
      subscribe(res => console.log("res %O", res),
                err => console.log("Error occured %O", err));
    return request;
  }

  updateTache(tache) {
    const request =  this.http.put(`${this.url}/tache`, tache).
      subscribe(res => console.log("res", res),
                err => console.log("Error occured ", err));
    return request;
  }

  deleteTache(id) {
    const request = this.http.delete(`${this.url}/tache/${id}`).
      subscribe(res => console.log("res %O", res),
                err => console.log("Error occured %O", err));
    return request;
  }

  getListTacheStatus() {
    return this.http.get(`${this.url}/tache-status`);
  }
}
