import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url = environment.url;

  constructor(private http: HttpClient) { };

  getUserDetails(auth) {
    return this.http.post(`${this.url}/auth`, auth);
  }

  validateToken(token) {
    return this.http.post(`${this.url}/auth/token`, token);
  }
}
