import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InterfaceComponent } from './interface/interface.component';
import { LoginComponent } from './login/login.component';
import { ListUtilisateurComponent } from './utilisateur/list-utilisateur/list-utilisateur.component';
import { FicheUtilisateurComponent } from './utilisateur/fiche-utilisateur/fiche-utilisateur.component';
import { ListProjetComponent } from './projet/list-projet/list-projet.component';
import { FicheProjetComponent } from './projet/fiche-projet/fiche-projet.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: 'connexion',
    component: LoginComponent,
  },
  { 
    path: '',
    redirectTo: 'connexion',
    pathMatch: 'full'
  },
  {
    path: '',
    component: InterfaceComponent,
    children: [
      {
        path: 'accueil',
        component: HomeComponent,
      },
      {
        path: 'utilisateur',
        children: [
          {
            path: '',
            component: ListUtilisateurComponent
          },
          {
            path: '',
            children: [
              {
                path: 'nouveau',
                component: FicheUtilisateurComponent,
              },
              {
                path: ':uid',
                component: FicheUtilisateurComponent,
              }
            ]
          }
        ]
      },
      {
        path: 'projet',
        children: [
          {
            path: '',
            component: ListProjetComponent
          },
          {
            path: '',
            children: [
              {
                path: 'nouveau',
                component: FicheProjetComponent,
              },
              {
                path: ':name',
                component: FicheProjetComponent,
              }
            ]
          }
        ]
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
